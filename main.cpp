
#include <iostream>
#include <vector>
#include <iterator>

#include <giolib/assert.h>
#include <giolib/exception.h>
#include <giolib/bitmask.h>

static bool VERBOSE = false;
static bool MEM_VERBOSE = false;
static int64_t VERB_INSTR_NUM = -1;
static int64_t VERB_LENGTH = -1;
static int64_t VERB_MULTIPLE = 0x0;

namespace gio::simplevm {

class execution_error : public std::exception {
public:
    execution_error(const std::string &what) : what_s(what) {}
    const char *what() const noexcept override {
        return this->what_s.c_str();
    }

private:
    const std::string what_s;
};

class memio {
public:
    typedef uint32_t mem_addr_type;
    typedef uint16_t io_port_type;

    static const mem_addr_type BEGIN_ADDR = 0x100000;
    static const mem_addr_type VIDEO_BEGIN = 0xb8000;
    static const mem_addr_type VIDEO_END = VIDEO_BEGIN + 2 * 80 * 25;

    memio(uint32_t mem_size) : memory(mem_size, 0) {}

    void load(std::istream &s) {
        std::copy(std::istreambuf_iterator<char>(s), std::istreambuf_iterator<char>(), this->memory.begin());
    }

    uint32_t read_mem(mem_addr_type addr, uint8_t size) {
        uint32_t val = this->read_mem_impl(addr, size);
        if (VERBOSE && MEM_VERBOSE) {
            std::cerr << this->mem_str(addr, size) << ": reading " << std::hex << val << std::endl;
        }
        return val;
    }

    void write_mem(mem_addr_type addr, uint8_t size, uint32_t val) {
        this->write_mem_impl(addr, size, val);
        if (VERBOSE && MEM_VERBOSE) {
            std::cerr << this->mem_str(addr, size) << ": writing " << std::hex << val << std::endl;
        }
    }

    uint32_t read_io(io_port_type port, uint8_t size) {
        uint32_t val = this->read_io_impl(port, size);
        if (VERBOSE && MEM_VERBOSE) {
            std::cerr << this->io_str(port, size) << ": reading " << std::hex << val << std::endl;
        }
        return val;
    }

    void write_io(io_port_type port, uint8_t size, uint32_t val) {
        this->write_io_impl(port, size, val);
        if (VERBOSE && MEM_VERBOSE) {
            std::cerr << this->io_str(port, size) << ": writing " << std::hex << val << std::endl;
        }
    }

private:
    template<typename T>
    static bool valid_addr(T addr, T begin, T end, uint8_t size) {
        return begin <= addr && addr <= end - size;
    }

    uint32_t read_mem_impl(mem_addr_type addr, uint8_t size) {
        this->check_mem_size(addr, size);

        if (valid_addr<mem_addr_type>(addr, BEGIN_ADDR, BEGIN_ADDR + this->memory.size(), size)) {
            void *host_addr = this->memory.data() + addr - BEGIN_ADDR;
            if (size == 1) return *static_cast<uint8_t*>(host_addr);
            if (size == 2) return *static_cast<uint16_t*>(host_addr);
            if (size == 4) return *static_cast<uint32_t*>(host_addr);
        }

        if (valid_addr<mem_addr_type>(addr, VIDEO_BEGIN, VIDEO_END, size)) {
            // Return zero on video memory reads
            return 0;
        }

        // Debug
        if (addr == 0x1001c && size == 4) {
            return 0x10020;
        }
        if (addr == 0x10020 && size == 1) {
            return 0xc3;
        }

        throw execution_error(this->mem_str(addr, size) + ": invalid read address");
    }

    void write_mem_impl(mem_addr_type addr, uint8_t size, uint32_t val) {
        this->check_mem_size(addr, size);

        if (valid_addr<mem_addr_type>(addr, BEGIN_ADDR, BEGIN_ADDR + this->memory.size(), size)) {
            void *host_addr = this->memory.data() + addr - BEGIN_ADDR;
            if (size == 1) *static_cast<uint8_t*>(host_addr) = static_cast<uint8_t>(val);
            if (size == 2) *static_cast<uint16_t*>(host_addr) = static_cast<uint16_t>(val);
            if (size == 4) *static_cast<uint32_t*>(host_addr) = val;
            return;
        }

        if (valid_addr<mem_addr_type>(addr, VIDEO_BEGIN, VIDEO_END, size)) {
            // Just drop video memory writes
            return;
        }

        // Debug
        if (0x10000 <= addr && addr <= 0x12000) {
            return;
        }

        throw execution_error(this->mem_str(addr, size) + ": invalid write address");
    }

    std::string mem_str(mem_addr_type addr, uint8_t size) const {
        std::ostringstream ss;
        ss << "access to memory address " << std::hex << addr << " of size " << std::dec << static_cast<uint32_t>(size);
        return ss.str();
    }

    void check_mem_size(mem_addr_type addr, uint8_t size) const {
        gio_assert_or_throw(size == 1 || size == 2 || size == 4, std::invalid_argument, this->mem_str(addr, size) + ": invalid size");
    }

    uint32_t read_io_impl(io_port_type port, uint8_t size) {
        this->check_io_size(port, size);

        if (port == 0x3fd) {
            // Serial port: read as QEMU
            return 0x60;
        }

        throw execution_error(this->io_str(port, size) + ": invalid read port");
    }

    void write_io_impl(io_port_type port, uint8_t size, uint32_t val) {
        this->check_io_size(port, size);

        if (0x3f8 <= port && port < 0x3f8+6) {
            // Serial port: just dump written characters
            if (port == 0x3f8) {
                std::cout << static_cast<char>(val) << std::flush;
            }
            return;
        }

        if ((port == 0x3d4 || port == 0x3d5) && size == 1) {
            // Terminal control port: ignore
            return;
        }

        if (port == 0x501) {
            // QEMU debug port: ignore
            return;
        }

        throw execution_error(this->io_str(port, size) + ": invalid write port");
    }

    std::string io_str(io_port_type port, uint8_t size) const {
        std::ostringstream ss;
        ss << "access to IO port " << std::hex << port << " of size " << std::dec << static_cast<uint32_t>(size);
        return ss.str();
    }

    void check_io_size(io_port_type port, uint8_t size) const {
        gio_assert_or_throw(size == 1 || size == 2 || size == 4, std::invalid_argument, this->io_str(port, size) + ": invalid size");
    }

    std::vector<uint8_t> memory;
};

class processor {
public:
    typedef uint32_t reg_type;

    processor(memio &memory) : eip(memio::BEGIN_ADDR), eflags(0), regs{}, memory(memory), instr_num(0) {}

    bool exec_step() {
        VERBOSE = false;
        if (this->instr_num >= VERB_INSTR_NUM) {
            VERBOSE = true;
            if (VERB_MULTIPLE == 0 || this->instr_num % VERB_MULTIPLE != 0) {
                VERBOSE = false;
            }
        }
        if (VERB_LENGTH >= 0 && this->instr_num == VERB_INSTR_NUM + VERB_LENGTH) {
            return false;
        }
        if (VERBOSE) {
            this->dump_state();
        }

        bool halt = false;
        uint8_t first = this->read(1);
        switch (first) {
        case 0x70: case 0x71: case 0x72: case 0x73:
        case 0x74: case 0x75: case 0x76: case 0x77:
        case 0x78: case 0x79: case 0x7a: case 0x7b:
        case 0x7c: case 0x7d: case 0x7e: case 0x7f: {
            // Conditional short jump
            uint8_t disp = this->read(1);
            if (this->eval_cc(first - 0x70)) {
                this->eip += sign_extend(disp, 1);
            }
            break;
        }

        case 0x40: case 0x41: case 0x42: case 0x43:
        case 0x44: case 0x45: case 0x46: case 0x47:
        case 0x48: case 0x49: case 0x4a: case 0x4b:
        case 0x4c: case 0x4d: case 0x4e: case 0x4f: {
            // Inc/dec register
            uint8_t reg = first & 0x7;
            bool inc = !(first & 0x8);
            modrm_data_t modrm = reg_to_modrm(reg);
            this->exec_inc(inc, modrm, 4);
            break;
        }

        case 0x50: case 0x51: case 0x52: case 0x53:
        case 0x54: case 0x55: case 0x56: case 0x57:
        case 0x58: case 0x59: case 0x5a: case 0x5b:
        case 0x5c: case 0x5d: case 0x5e: case 0x5f: {
            // Push/pop register
            uint8_t reg = first & 0x7;
            bool push = !(first & 0x8);
            if (push) {
                reg_type val = this->load_reg(reg,4);
                this->push(val);
            } else {
                reg_type val = this->pop();
                this->store_reg(reg, 4, val);
            }
            break;
        }

        case 0x6a: {
            // Push imm8
            reg_type val = sign_extend(this->read(1), 1);
            this->push(val);
            break;
        }

        case 0x68: {
            // Push imm32
            reg_type val = this->read(4);
            this->push(val);
            break;
        }

        case 0x8f: {
            modrm_data_t modrm = this->decode_modrm();
            switch (modrm.reg) {
            case 0: {
                // Pop memory
                reg_type val = this->pop();
                this->store_modrm(modrm, 4, val);
                break;
            }
            default:
                throw execution_error(gio_make_string("invalid operation: " << static_cast<uint32_t>(modrm.reg)));
            }
            break;
        }

        case 0x00: case 0x01: case 0x02: case 0x03: case 0x04: case 0x05:
        case 0x10: case 0x11: case 0x12: case 0x13: case 0x14: case 0x15:
        case 0x20: case 0x21: case 0x22: case 0x23: case 0x24: case 0x25:
        case 0x30: case 0x31: case 0x32: case 0x33: case 0x34: case 0x35:
        case 0x08: case 0x09: case 0x0a: case 0x0b: case 0x0c: case 0x0d:
        case 0x18: case 0x19: case 0x1a: case 0x1b: case 0x1c: case 0x1d:
        case 0x28: case 0x29: case 0x2a: case 0x2b: case 0x2c: case 0x2d:
        case 0x38: case 0x39: case 0x3a: case 0x3b: case 0x3c: case 0x3d: {
            // Group 1 register
            uint8_t size = first & 0x1 ? 4 : 1;
            uint8_t op = first >> 3;
            uint8_t var = (first >> 1) & 0x3;
            modrm_data_t modrm;
            reg_type op1;
            reg_type op2;
            if (var < 2) {
                modrm = this->decode_modrm();
            }
            switch (var) {
            case 0:
                op1 = this->load_modrm(modrm, size);
                op2 = this->load_reg(modrm.reg, size);
                break;
            case 1:
                op1 = this->load_reg(modrm.reg, size);
                op2 = this->load_modrm(modrm, size);
                modrm = this->reg_to_modrm(modrm.reg);
                break;
            case 2:
                modrm = this->reg_to_modrm(EAX);
                op1 = this->load_modrm(modrm, size);
                op2 = this->read(size);
                break;
            default:
                gio_should_not_arrive_here();
            }
            this->exec_op(op, op1, op2, modrm, size);
            break;
        }

        case 0x80: case 0x81: case 0x83: {
            // Group 1 immediate
            uint8_t size = first & 0x1 ? 4 : 1;
            modrm_data_t modrm = this->decode_modrm();
            reg_type op1 = this->load_modrm(modrm, size);
            reg_type op2;
            uint8_t op = modrm.reg;
            if (first == 0x83) {
                op2 = sign_extend(this->read(1), 1);
            } else {
                op2 = this->read(size);
            }
            this->exec_op(op, op1, op2, modrm, size);
            break;
        }

        case 0x84: case 0x85: {
            // Test
            uint8_t size = first & 0x1 ? 4 : 1;
            modrm_data_t modrm = this->decode_modrm();
            reg_type op1 = this->load_modrm(modrm, size);
            reg_type op2 = this->load_reg(modrm.reg, size);
            this->exec_op((0x3 << 3) | 0x0, op1, op2, modrm, size);
            break;
        }

        case 0x88: case 0x89: case 0x8a: case 0x8b: {
            // Mov
            uint8_t size = first & 0x01 ? 4 : 1;
            auto modrm = this->decode_modrm();
            if (first >= 0x8a) {
                this->store_reg(modrm.reg, size, this->load_modrm(modrm, size));
            } else {
                this->store_modrm(modrm, size, this->load_reg(modrm.reg, size));
            }
            break;
        }

        case 0x8d: {
            // Lea
            auto modrm = this->decode_modrm();
            gio_assert_or_throw(modrm.mem, execution_error, "not a memory reference");
            this->store_reg(modrm.reg, 4, modrm.addr);
            break;
        }

        case 0x99: {
            // Cdq
            reg_type sign = this->load_reg(EAX, 4) >> 31;
            this->store_reg(EDX, 4, sign * 0xffffffff);
            break;
        }

        case 0xc6: case 0xc7: {
            // Group 11
            uint8_t size = first & 0x01 ? 4 : 1;
            auto modrm = this->decode_modrm();
            switch (modrm.reg) {
            case 0x0: {
                // Mov immediate
                reg_type val = this->read(size);
                this->store_modrm(modrm, size, val);
                break;
            }
            default:
                throw execution_error(gio_make_string("invalid operation: " << static_cast<uint32_t>(modrm.reg)));
            }
            break;
        }

        case 0xb0: case 0xb1: case 0xb2: case 0xb3:
        case 0xb4: case 0xb5: case 0xb6: case 0xb7:
        case 0xb8: case 0xb9: case 0xba: case 0xbb:
        case 0xbc: case 0xbd: case 0xbe: case 0xbf: {
            // Mov immediate
            uint8_t size = first & 0x8 ? 4 : 1;
            uint32_t val = this->read(size);
            uint8_t reg = first & 0x7;
            this->store_reg(reg, size, val);
            break;
        }

        case 0xa0: case 0xa1: {
            // Mov to EAX
            uint8_t size = first & 0x01 ? 4 : 1;
            uint32_t addr = this->read(4);
            reg_type val = this->memory.read_mem(addr, size);
            this->store_reg(EAX, size, val);
            break;
        }

        case 0xa2: case 0xa3: {
            // Mov from EAX
            uint8_t size = first & 0x01 ? 4 : 1;
            uint32_t addr = this->read(4);
            reg_type val = this->load_reg(EAX, size);
            this->memory.write_mem(addr, size, val);
            break;
        }

        case 0xc0: case 0xc1:
        case 0xd0: case 0xd1: case 0xd2: case 0xd3: {
            // Group 2
            uint8_t size = first & 0x01 ? 4 : 1;
            modrm_data_t modrm = this->decode_modrm();
            uint8_t count;
            if ((first & 0xfe) == 0xc0) {
                count = this->read(1);
            } else if ((first & 0xfe) == 0xd0) {
                count = 1;
            } else if ((first & 0xfe) == 0xd2) {
                count = this->load_reg(ECX, 1);
            } else {
                gio_should_not_arrive_here();
            }
            count &= 0x1f;
            if (count == 0) {
                break;
            }
            reg_type op1 = this->load_modrm(modrm, size);
            uint8_t overflow;
            switch (modrm.reg) {
            case 0x4:
                // Sal, shl
                CARRY.set<reg_type>(this->eflags, (op1 << (count-1)) >> (8*size-1));
                overflow = ((op1 >> (8*size-1)) ^ (op1 >> (8*size-2))) & 0x1;
                op1 = op1 << count;
                break;
            case 0x5:
                // Shr
                CARRY.set<reg_type>(this->eflags, static_cast<uint32_t>(op1) >> (count-1));
                overflow = op1 >> (8*size-1);
                op1 = static_cast<uint32_t>(op1) >> count;
                break;
            case 0x7:
                // Sar
                CARRY.set<reg_type>(this->eflags, static_cast<int32_t>(op1) >> (count-1));
                overflow = 0;
                op1 = static_cast<int32_t>(op1) >> count;
                break;
            default:
                throw execution_error(gio_make_string("invalid operation: " << static_cast<uint32_t>(modrm.reg)));
            }
            if (count == 1) {
                OVERFLOW.set<reg_type>(this->eflags, overflow);
            }
            this->store_modrm(modrm, size, op1);
            this->set_zsf(op1, size);
            break;
        }

        case 0xe8: {
            // Call
            reg_type disp = this->read(4);
            this->push(this->eip);
            this->eip += disp;
            break;
        }

        case 0xc3: {
            // Ret
            this->eip = this->pop();
            break;
        }

        case 0xeb: {
            // Short jump
            uint8_t disp = this->read(1);
            this->eip += sign_extend(disp, 1);
            break;
        }

        case 0xe9: {
            // Jump
            reg_type disp = this->read(4);
            this->eip += disp;
            break;
        }

        case 0xec: case 0xed: {
            // In
            uint8_t size = first & 0x01 ? 4 : 1;
            reg_type port_num = this->load_reg(EDX, 2);
            reg_type val = this->memory.read_io(port_num, size);
            this->store_reg(EAX, size, val);
            break;
        }

        case 0xee: case 0xef: {
            // Out
            uint8_t size = first & 0x01 ? 4 : 1;
            reg_type port_num = this->load_reg(EDX, 2);
            reg_type val = this->load_reg(EAX, size);
            this->memory.write_io(port_num, size, val);
            break;
        }

        // 8-bit version is disabled: div and idiv code is broken for the moment
        //case 0xf6:
        case 0xf7: {
            // Group 3
            uint8_t size = first & 0x01 ? 4 : 1;
            auto modrm = this->decode_modrm();
            switch (modrm.reg) {
            case 0x4: {
                // Mul
                uint64_t op1 = zero_extend(this->load_reg(EAX, size), size);
                uint64_t op2 = zero_extend(this->load_modrm(modrm, size), size);
                uint64_t val = op1 * op2;
                if (size == 1) {
                    this->store_reg(EAX, 2, val);
                } else if (size == 4) {
                    this->store_reg(EAX, 4, val);
                    this->store_reg(EDX, 4, val >> 32);
                } else {
                    throw execution_error("invalid size");
                }
                bool upper = val != zero_extend(val, size);
                CARRY.set<reg_type>(this->eflags, upper);
                OVERFLOW.set<reg_type>(this->eflags, upper);
                break;
            }
            case 0x5: {
                // Imul
                int64_t op1 = sign_extend(this->load_reg(EAX, size), size);
                int64_t op2 = sign_extend(this->load_modrm(modrm, size), size);
                int64_t val = op1 * op2;
                if (size == 1) {
                    this->store_reg(EAX, 2, val);
                } else if (size == 4) {
                    this->store_reg(EAX, 4, val);
                    this->store_reg(EDX, 4, val >> 32);
                } else {
                    throw execution_error("invalid size");
                }
                bool upper = val != sign_extend(val, size);
                CARRY.set<reg_type>(this->eflags, upper);
                OVERFLOW.set<reg_type>(this->eflags, upper);
                break;
            }
            case 0x6: {
                // Div
                uint64_t op1 = this->load_reg(EAX, size) + (static_cast<uint64_t>(this->load_reg(EDX, size)) << 32);
                uint64_t op2 = this->load_modrm(modrm, size);
                gio_assert_or_throw(op2 != 0, execution_error, "division by zero");
                uint64_t div = op1 / op2;
                gio_assert_or_throw(div == static_cast<uint32_t>(div), execution_error, "overflowing division");
                uint64_t mod = op1 & op2;
                this->store_reg(EAX, size, div);
                this->store_reg(EDX, size, mod);
                break;
            }
            case 0x7: {
                // Idiv
                int64_t op1 = this->load_reg(EAX, size) + (static_cast<int64_t>(this->load_reg(EDX, size)) << 32);
                int64_t op2 = this->load_modrm(modrm, size);
                gio_assert_or_throw(op2 != 0, execution_error, "division by zero");
                int64_t div = op1 / op2;
                gio_assert_or_throw(div == static_cast<int32_t>(div), execution_error, "overflowing division");
                int64_t mod = op1 % op2;
                this->store_reg(EAX, size, div);
                this->store_reg(EDX, size, mod);
                break;
            }
            default:
                throw execution_error(gio_make_string("invalid operation: " << static_cast<uint32_t>(modrm.reg)));
            }
            break;
        }

        case 0xfa: {
            // Clear interrupts (ignored)
            break;
        }

        case 0xfe: case 0xff: {
            // Group 4 and 5
            uint8_t size = first & 0x01 ? 4 : 1;
            auto modrm = this->decode_modrm();
            gio_assert_or_throw(first == 0xff || modrm.reg < 2, execution_error, "invalid opcode/operation");
            switch (modrm.reg) {
            case 0x0: case 0x1: {
                // Inc, dec
                bool inc = !(modrm.reg & 0x1);
                this->exec_inc(inc, modrm, size);
                break;
            }
            case 0x2: {
                // Call memory
                reg_type loc = this->load_modrm(modrm, 4);
                this->push(this->eip);
                this->eip = loc;
                break;
            }
            case 0x6: {
                // Push memory
                reg_type val = this->load_modrm(modrm, 4);
                this->push(val);
                break;
            }
            default:
                throw execution_error(gio_make_string("invalid operation: " << static_cast<uint32_t>(modrm.reg)));
            }
            break;
        }

        case 0x0f: {
            uint8_t second = this->read(1);
            switch (second) {
            case 0xc8: case 0xc9: case 0xca: case 0xcb:
            case 0xcc: case 0xcd: case 0xce: case 0xcf: {
                // Bswap
                uint8_t reg = second - 0xc8;
                reg_type val = this->load_reg(reg, 4);
                val = (val & 0xff) << 24 | ((val >> 8) & 0xff) << 16 | ((val >> 16) & 0xff) << 8 | ((val >> 24) & 0xff);
                this->store_reg(reg, 4, val);
                break;
            }

            case 0x80: case 0x81: case 0x82: case 0x83:
            case 0x84: case 0x85: case 0x86: case 0x87:
            case 0x88: case 0x89: case 0x8a: case 0x8b:
            case 0x8c: case 0x8d: case 0x8e: case 0x8f: {
                // Conditional jump
                reg_type disp = this->read(4);
                if (this->eval_cc(second - 0x80)) {
                    this->eip += disp;
                }
                break;
            }

            default:
                throw execution_error(gio_make_string("unknown opcode second byte " << static_cast<uint32_t>(second)));
            }
            break;
        }

        case 0xf4: {
            // Hlt
            halt = true;
            break;
        }

        default:
            throw execution_error(gio_make_string("unknown opcode first byte " << static_cast<uint32_t>(first)));
        }

        this->instr_num++;

        return !halt;
    }

    void exec() {
        try {
            while (this->exec_step());
        } catch (execution_error &e) {
            this->die(e.what());
        }

        std::cerr << "Halting after " << std::hex << this->instr_num << " instructions!\n";
    }

private:
    void dump_state() {
        std::cerr << "instr_num=" << std::hex << std::setw(8) << std::setfill('0') << this->instr_num << " ";
        std::cerr << "EIP=" << std::hex << std::setw(8) << std::setfill('0') << this->eip << " ";
        std::cerr << "EAX=" << std::hex << std::setw(8) << std::setfill('0') << this->regs[EAX] << " ";
        std::cerr << "ECX=" << std::hex << std::setw(8) << std::setfill('0') << this->regs[ECX] << " ";
        std::cerr << "EDX=" << std::hex << std::setw(8) << std::setfill('0') << this->regs[EDX] << " ";
        std::cerr << "EBX=" << std::hex << std::setw(8) << std::setfill('0') << this->regs[EBX] << " ";
        std::cerr << "ESP=" << std::hex << std::setw(8) << std::setfill('0') << this->regs[ESP] << " ";
        std::cerr << "EBP=" << std::hex << std::setw(8) << std::setfill('0') << this->regs[EBP] << " ";
        std::cerr << "ESI=" << std::hex << std::setw(8) << std::setfill('0') << this->regs[ESI] << " ";
        std::cerr << "EDI=" << std::hex << std::setw(8) << std::setfill('0') << this->regs[EDI] << " ";
        std::cerr << "EFLAGS=" << std::hex << std::setw(8) << std::setfill('0') << this->eflags;
        std::cerr << " (" << (CARRY.decode(this->eflags) ? "C" : "c");
        std::cerr << " " << (ZERO.decode(this->eflags) ? "Z" : "z");
        std::cerr << " " << (SIGN.decode(this->eflags) ? "S" : "s");
        std::cerr << " " << (OVERFLOW.decode(this->eflags) ? "O" : "o") << ")\n";
        //std::cerr.flush();
    }

    [[noreturn]] void die(const std::string &reason) {
        std::cerr << "Execution error at instruction number " << std::hex << this->instr_num << ": " << reason << std::endl;
        this->dump_state();
        exit(2);
    }

    struct modrm_data_t {
        bool mem;
        uint8_t reg;
        uint8_t reg2;
        reg_type addr;
    };

    bool eval_cc(uint8_t cc) {
        switch (cc) {
        case 0x0:
            return OVERFLOW.decode<reg_type>(this->eflags);
        case 0x1:
            return !OVERFLOW.decode<reg_type>(this->eflags);
        case 0x2:
            return CARRY.decode<reg_type>(this->eflags);
        case 0x3:
            return !CARRY.decode<reg_type>(this->eflags);
        case 0x4:
            return ZERO.decode<reg_type>(this->eflags);
        case 0x5:
            return !ZERO.decode<reg_type>(this->eflags);
        case 0x6:
            return CARRY.decode<reg_type>(this->eflags) || ZERO.decode<reg_type>(this->eflags);
        case 0x7:
            return !CARRY.decode<reg_type>(this->eflags) && !ZERO.decode<reg_type>(this->eflags);
        case 0x8:
            return SIGN.decode<reg_type>(this->eflags);
        case 0x9:
            return !SIGN.decode<reg_type>(this->eflags);
            // Parity codes (0xa and 0xb) not implemented
        case 0xc:
            return !SIGN.decode<reg_type>(this->eflags) ^ !OVERFLOW.decode<reg_type>(this->eflags);
        case 0xd:
            return SIGN.decode<reg_type>(this->eflags) ^ !OVERFLOW.decode<reg_type>(this->eflags);
        case 0xe:
            return (!SIGN.decode<reg_type>(this->eflags) ^ !OVERFLOW.decode<reg_type>(this->eflags)) || ZERO.decode<reg_type>(this->eflags);
        case 0xf:
            return (SIGN.decode<reg_type>(this->eflags) ^ !OVERFLOW.decode<reg_type>(this->eflags)) && !ZERO.decode<reg_type>(this->eflags);
        default:
            throw execution_error(gio_make_string("invalid condition code: " << static_cast<uint32_t>(cc)));
        }
    }

    void exec_op(uint8_t op, reg_type op1, reg_type op2, modrm_data_t modrm, uint8_t size) {
        bool store = true;
        switch (op) {
        case 0x0: {
            // Add
            uint64_t uop1 = op1;
            uint64_t uop2 = op2;
            int64_t sop1 = op1;
            int64_t sop2 = op2;
            uop1 += uop2;
            sop1 += sop2;
            op1 = zero_extend(sop1, size);
            gio_assert(op1 == zero_extend(uop1, size));
            CARRY.set<reg_type>(this->eflags, uop1 != static_cast<uint32_t>(op1));
            OVERFLOW.set<reg_type>(this->eflags, sop1 != static_cast<int32_t>(op1));
            break;
        }
        case 0x1: {
            // Or
            op1 |= op2;
            CARRY.set<reg_type>(this->eflags, 0);
            OVERFLOW.set<reg_type>(this->eflags, 0);
            break;
        }
        case 0x4: case (0x3 << 3) | 0x0: {
            // And
            op1 &= op2;
            CARRY.set<reg_type>(this->eflags, 0);
            OVERFLOW.set<reg_type>(this->eflags, 0);
            if (op == ((0x3 << 3) | 0x0)) {
                store = false;
            }
            break;
        }
        case 0x5: case 0x7: {
            // Sub, Cmp
            uint64_t uop1 = op1;
            uint64_t uop2 = op2;
            int64_t sop1 = op1;
            int64_t sop2 = op2;
            uop1 -= uop2;
            sop1 -= sop2;
            op1 = zero_extend(sop1, size);
            gio_assert(op1 == zero_extend(uop1, size));
            CARRY.set<reg_type>(this->eflags, uop1 != static_cast<uint32_t>(op1));
            OVERFLOW.set<reg_type>(this->eflags, sop1 != static_cast<int32_t>(op1));
            if (op == 0x7) {
                store = false;
            }
            break;
        }
        case 0x6: {
            // Xor
            op1 ^= op2;
            CARRY.set<reg_type>(this->eflags, 0);
            OVERFLOW.set<reg_type>(this->eflags, 0);
            break;
        }
        default:
            throw execution_error(gio_make_string("invalid operation: " << static_cast<uint32_t>(op)));
        }
        if (store) {
            this->store_modrm(modrm, size, op1);
        }
        this->set_zsf(op1, size);
    }

    void exec_inc(bool inc, modrm_data_t modrm, uint8_t size) {
        reg_type val = this->load_modrm(modrm, size);
        if (inc) {
            OVERFLOW.set<reg_type>(this->eflags, val == ~(1 << (8*size-1)));
            val++;
        } else {
            OVERFLOW.set<reg_type>(this->eflags, val == (1 << (8*size-1)));
            val--;
        }
        this->store_modrm(modrm, size, val);
        this->set_zsf(val, size);
    }

    void set_zsf(reg_type val, uint8_t size) {
        if (size == 1) {
            val &= 0xff;
            ZERO.set<reg_type>(this->eflags, val == 0);
            SIGN.set<reg_type>(this->eflags, val >> 7);
        } else if (size == 2) {
            val &= 0xffff;
            ZERO.set<reg_type>(this->eflags, val == 0);
            SIGN.set<reg_type>(this->eflags, val >> 15);
        } else if (size == 4) {
            ZERO.set<reg_type>(this->eflags, val == 0);
            SIGN.set<reg_type>(this->eflags, val >> 31);
        } else {
            throw std::invalid_argument("invalid size");
        }
    }

    void push(reg_type val) {
        this->regs[ESP] -= 4;
        this->memory.write_mem(this->regs[ESP], 4, val);
    }

    reg_type pop() {
        reg_type ret = this->memory.read_mem(this->regs[ESP], 4);
        this->regs[ESP] += 4;
        return ret;
    }

    template<typename T>
    static uint64_t sign_extend(T x, uint8_t size) {
        if (size == 1) {
            return static_cast<reg_type>(static_cast<int8_t>(x));
        } else if (size == 4) {
            return static_cast<reg_type>(static_cast<int32_t>(x));
        }
        throw std::invalid_argument("invalid size");
    }

    template<typename T>
    static uint64_t zero_extend(T x, uint8_t size) {
        if (size == 1) {
            return static_cast<reg_type>(static_cast<uint8_t>(x));
        } else if (size == 4) {
            return static_cast<reg_type>(static_cast<uint32_t>(x));
        }
        throw std::invalid_argument("invalid size");
    }

    reg_type read(uint8_t size) {
        auto ret = this->memory.read_mem(eip, size);
        this->eip += size;
        return ret;
    }

    reg_type load_reg(uint8_t reg, uint8_t size) {
        if (size == 4) {
            return this->regs[reg];
        } else if (size == 2) {
            return this->regs[reg] & 0xffff;
        } else if (size == 1) {
            return this->regs[reg] & 0xff;
        }
        throw std::invalid_argument("invalid size");
    }

    void store_reg(uint8_t reg, uint8_t size, reg_type val) {
        if (size == 4) {
            this->regs[reg] = val;
            return;
        } else if (size == 2) {
            this->regs[reg] = (this->regs[reg] & 0xffff0000) | (val & 0xffff);
        } else if (size == 1) {
            this->regs[reg] = (this->regs[reg] & 0xffffff00) | (val & 0xff);
            return;
        }
        throw std::invalid_argument("invalid size");
    }

    reg_type load_modrm(modrm_data_t modrm, uint8_t size) {
        if (modrm.mem) {
            return this->memory.read_mem(modrm.addr, size);
        } else {
            return this->load_reg(modrm.reg2, size);
        }
    }

    void store_modrm(modrm_data_t modrm, uint8_t size, reg_type val) {
        if (modrm.mem) {
            this->memory.write_mem(modrm.addr, size, val);
        } else {
            this->store_reg(modrm.reg2, size, val);
        }
    }

    static modrm_data_t reg_to_modrm(uint8_t reg) {
        return {false, 0, reg, 0};
    }

    modrm_data_t decode_modrm() {
        auto modrm = this->read(1);
        uint8_t mod = modrm >> 6;
        uint8_t reg = (modrm >> 3) & 0x7;
        uint8_t rm = modrm & 0x7;

        if (mod == 0x3) {
            return {false, reg, rm, 0};
        }

        reg_type addr = 0;
        bool force_disp32 = false;
        if (rm == 0x4) {
            // There is a SIB
            auto sib = this->read(1);
            uint8_t scale = sib >> 6;
            uint8_t index = (sib >> 3) & 0x7;
            uint8_t base = sib & 0x7;

            if (index != 0x4) {
                addr += this->regs[index] << scale;
            }

            if (base != 0x5 || scale != 0x0) {
                addr += this->regs[base];
            }
        } else {
            // There is no SIB
            if (mod == 0x0 && rm == 0x5) {
                force_disp32 = true;
            } else {
                addr += this->regs[rm];
            }
        }

        if (mod == 0x1 && !force_disp32) {
            auto disp = this->read(1);
            addr += sign_extend(disp, 1);
        }

        if (mod == 0x2 || force_disp32) {
            auto disp = this->read(4);
            addr += disp;
        }

        return {true, reg, 0, addr};
    }

    static const uint8_t EAX = 0;
    static const uint8_t ECX = 1;
    static const uint8_t EDX = 2;
    static const uint8_t EBX = 3;
    static const uint8_t ESP = 4;
    static const uint8_t EBP = 5;
    static const uint8_t ESI = 6;
    static const uint8_t EDI = 7;

    static constexpr bitmask CARRY{0, 1};
    static constexpr bitmask ZERO{6, 7};
    static constexpr bitmask SIGN{7, 8};
    static constexpr bitmask OVERFLOW{11, 12};

    reg_type eip;
    reg_type eflags;
    std::array<reg_type, 8> regs;
    memio &memory;
    int64_t instr_num;
};

int main(int argc, char *argv[]) {
    install_exception_handler();

    if (argc != 2) {
        std::cerr << "Please specify image file name...\n";
        return 1;
    }
    const char *filename = argv[1];

    memio memory(256 * 1024 * 1024);
    {
        std::ifstream fin(filename);
        memory.load(fin);
    }
    processor proc(memory);

    proc.exec();

    return 0;
}

}

int main(int argc, char *argv[]) {
    return gio::simplevm::main(argc, argv);
}
